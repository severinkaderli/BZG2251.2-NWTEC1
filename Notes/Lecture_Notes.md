---
title: "BZG2251.2-NWTEC1"
subtitle: "Geometrische Optik und deren Anwendungen"
author:
    - Severin Kaderli
extra-info: true
institute: "Berner Fachhochschule"
department: "Technik und Informatik"
lecturer: "Jan Locher"
lang: "de-CH"
lof: true
...

# Modulinformationen

## Prüfung

* Modulprüfung am 11. Januar 2019

## Modulunterlagen
* [Moodle](https://moodle.bfh.ch/course/view.php?id=18719) (Einschreibeschlüssel ist: **prisma**)

# Licht
Licht benötigt kein Ausbreitungsmedium. Die Lichtgeschwindigkeit beträgt im
Vakuum $c = \SI{299792458}{m/s}$. 

# Reflexion
## Reflexionsgesetz
Der Einfallswinkel ist gleich dem Reflexionswinkel.

$$
\Theta_{i} = \Theta_{r}
$$

## Brechungsgesetz
Ein Lichtstrahl wird gebrochen, wenn er von einem transparenten Medium in ein
anderes transparentes Medium übergeht.

$$
n_{1} \cdot sin(\Theta_{i}) = n_{2} \cdot sin(\Theta_{r})
$$

## Reflexionsgrad
Mithilfe der frensnelschen Formeln kann man berechnen, welcher Anteil der
Lichtleistung von einem Medium reflektiert wird, bei einem senkrechten Einfall. Dies nennt man den Reflexionsgrad $R$.

$$
R = (\frac{n_{1} - n_{2}}{n_{1} + n_{2}})^{2}
$$

## Dispersion
Durch verschiedene Wellenlängen werden die verschiedenen Farben eines
Lichtstrahles anders gebrochen. Diesen Vorgang nennt man Dispersion.

## Totalreflexion
Die Totalreflexion ist der Fall, wenn 100% Reflektivität gegeben ist. Der
kritische Winkel $\Theta_{k}$ ist folgendermassen gegeben:

$$
\Theta_{k} = arcsin(\frac{n_{2}}{n_{1}})
$$

# Abbildungen
Eine Abbildung ist ein Bild von einem Objekt, die mit einem Mechanismus oder einem Gerät erstellt wird.

## Camera Obscura (Lochkamera)
Bei einer Lochkamera fehlt eine Linse und ein Loch alleine ist für die Abbildung zuständig. Die Strahlen eines Objektes gelangen durch die Blende auf den Schirm und dadurch entsteht eine Abbildung des Objektes. 

Wenn die Blende klein gewählt wird, wird das Bild scharf. Bei zunehmendem Abstand des Schirmes wird das Bild grösser. Jedoch ist das Bild sehr dunkel, da nur sehr wenig Licht durch die Blende gelangt.

Um das Problem der Lochkamera zu beheben, kann man eine Linse verwenden, die das Licht bündelt und an einem Punkt sammelt.

## Spiegel 
### Wölbspiegel
Auch mit einem Wölbspiegel kann man Licht sammeln und dadurch Abbildungen
erzeugen. Bei so einem Spiegel befindet sich der Krümmungsmittelpunkt und der
Brennpunkt hinter dem Spiegel. Der Krümmungsradius $R$ und die Brennweite $f$
sind deshalb negativ.

![Wölbspiegel](./assets/woelbspiegel.png)

### Ebener Spiegel
Auch mit einem ebenen Spiegel kann man eine Abbildung erstellen. Jedoch handelt es sich dabei um eine virtuelle Abbildung.

![Ebener Spiegel](./assets/ebener_spiegel.png)

## Sphärische Spiegel


## Bildarten
### Reele Bilder
Reelle Bilder zeichnen sich dadurch aus, dass die Lichtstrahlen durch
die Bildpunkte gehen. Hält man ein Blatt Papier an den Ort des
projizierten Bildes, so sieht man ein Bild.

### Virtuelle Bilder
Virtuelle Bilder eines Gegenstandes erzeugen Lichtstrahlen, welche so
verlaufen, als ob an der entsprechenden Stelle ein Gegenstand oder auch
ein reelles Bild wäre. Auf einem Blatt Papier entsteht kein Bild.

## Abbildungsgleichung
$R$: Krümmungsradius  
$G$: Gegenstandspunkt  
$g$: Gegenstandsweite  
$B$: Bildpunkt  
$b$: Bildweite  
$S$: Scheitelpunkt  
$M$: Krümmungsmittelpunkt

$$
\frac{1}{f} = \frac{1}{b} + \frac{1}{g}
$$

Bei einem Hohlspiegel entspricht die Brennweite der Hälfte seines
Krümmungsradius.

Zusätzlich gilt die folgende Beziehung:

$$
\frac{B}{G} = -\frac{b}{g}
$$

## Lateralvergrösserung
Bei einem Spiegel wird ein Gegenstand $G$ um den Faktor $V$ vergrössert.

$$
V = -\frac{b}{g}
$$

# Sphärische Flächen und Linsen
## Abbildungen
Bei spährischen Flächen folgende Abbildungsgleichung:

$$
\frac{n_{1}}{g} + \frac{n_{2}}{b} = \frac{n_{2}-n_{1}}{r}
$$

Bei Linsen benötigt man folgende Gleichung:

$$
\frac{1}{f} = (n - 1) \cdot (\frac{1}{r_{1}} - \frac{1}{r_{2}})
$$

## Brechkraft
Den Kehrwert der Brennweite einer Linse nennt man Brechkraft und die Einheit
davon ist die Dioptrie $\si{D}$.

$$
B = \frac{1}{f}
$$


## Vergrösserung
Auch hier benötigt man eine andere Formel:

$$
V = \frac{B}{G} = -\frac{n_{1} \cdot b}{n_{2} \cdot g}
$$

# Das Auge
![Das Auge](./assets/auge.png) 

## Retina
Auf der Retina (Netzhaut) wird das Licht in elektrische Impulse umgewandelt.

Die Retina enthält die Rezeptorzellen (Stäbchen und Zapfen). Die Stäbchen sind
die Photorezeptoren für das Nachtsehen (ca. 120 Mio.). Die Zapfen sind die
Photorezeptoren für farbigeses Sehen (ca. 6 Mio). Von den Zapfen gibt es 3
Typen: rot, grün und blau. Verschiedene Zellen anderer Art dienen dem
Preprocessing bevor das Signal über das Nervenbündel ins Gehirn gelangt.

### Gelber Fleck / Blinder Fleck
Der gelbe Fleck ist der bereich der Netzhaut mit der grösstn Dichte von
Sehzellen. Der blinde Fleck ist der Fleck an dem der Sehrnerv, die
Zentralarterie und die Zentralvene in das Auge ein- und austreten.

## Nahpunkt
Den Punkt mit der kleinsten Entfernung, an die sich die Linse anpassen kann.

## Fernpunkt
Der am weitesten entfernte Punkt, den dieser Mensch noch scharf sehen kann. Bei
einem normalsichtigen Auge ist der Nahpunkt bei $\SI{25}{cn}$ und der Fernpunkt
bei unendlich.

## Weitsichtigkeit
Bei der Weitsichtigkeit werden entferne Gegenstände scharf wahrgenommen. Sie
kann durch eine Sammellinse korrigiert werden, da dadurch die Brennweite
verkürzt wird.

## Kurzsichtigkeit
Bei der Kurzsichtigkeit werden nur nahe Objekte fokussiert. Sie kann durch eine
Zerstreuungslinse korrigiert werden, damit das Bild weiter hinten, nämlich auf
der Netzhaut, fokussiert wird.